module gitee.com/chunanyong/zorm-examples

go 1.13

require (
	gitee.com/chunanyong/dm v1.8.17
	gitee.com/chunanyong/zorm v1.7.7
	github.com/cectc/hptx v1.0.5
	github.com/go-sql-driver/mysql v1.8.1
)
